\begin{thebibliography}{12}
\expandafter\ifx\csname natexlab\endcsname\relax\def\natexlab#1{#1}\fi

\bibitem[{Bordes et~al.(2014)Bordes, Glorot, Weston, and Bengio}]{wn18}
Antoine Bordes, Xavier Glorot, Jason Weston, and Yoshua Bengio. 2014.
\newblock A semantic matching energy function for learning with
  multi-relational data.
\newblock \emph{Machine Learning}, 94(2):233--259.

\bibitem[{Bordes et~al.(2013)Bordes, Usunier, Garcia-Duran, Weston, and
  Yakhnenko}]{transe}
Antoine Bordes, Nicolas Usunier, Alberto Garcia-Duran, Jason Weston, and Oksana
  Yakhnenko. 2013.
\newblock \href
  {http://papers.nips.cc/paper/5071-translating-embeddings-for-modeling-multi-relational-data.pdf}
  {Translating embeddings for modeling multi-relational data}.
\newblock In C.~J.~C. Burges, L.~Bottou, M.~Welling, Z.~Ghahramani, and K.~Q.
  Weinberger, editors, \emph{Advances in Neural Information Processing Systems
  26}, pages 2787--2795. Curran Associates, Inc., Lake Tahoe, USA.

\bibitem[{Bordes et~al.(2011)Bordes, Weston, Collobert, and
  Bengio}]{linkprediction}
Antoine Bordes, Jason Weston, Ronan Collobert, and Yoshua Bengio. 2011.
\newblock \href {http://dl.acm.org/citation.cfm?id=2900423.2900470} {Learning
  structured embeddings of knowledge bases}.
\newblock In \emph{Proceedings of the Twenty-Fifth AAAI Conference on
  Artificial Intelligence}, AAAI'11, pages 301--306, San Francisco, California.
  AAAI Press.

\bibitem[{Dettmers et~al.(2018)Dettmers, Pasquale, Pontus, and Riedel}]{conve}
Tim Dettmers, Minervini Pasquale, Stenetorp Pontus, and Sebastian Riedel. 2018.
\newblock \href {https://arxiv.org/abs/1707.01476} {Convolutional 2d knowledge
  graph embeddings}.
\newblock In \emph{Proceedings of the 32th AAAI Conference on Artificial
  Intelligence}, pages 1811--1818.

\bibitem[{Ji et~al.(2015)Ji, He, Xu, Liu, and Zhao}]{transd}
Guoliang Ji, Shizhu He, Liheng Xu, Kang Liu, and Jun Zhao. 2015.
\newblock Knowledge graph embedding via dynamic mapping matrix.
\newblock In \emph{{ACL} {(1)}}, pages 687--696, Beijing, China. The
  Association for Computer Linguistics.

\bibitem[{Lin et~al.(2015)Lin, Liu, Sun, Liu, and Zhu}]{transr}
Yankai Lin, Zhiyuan Liu, Maosong Sun, Yang Liu, and Xuan Zhu. 2015.
\newblock \href {http://dl.acm.org/citation.cfm?id=2886521.2886624} {Learning
  entity and relation embeddings for knowledge graph completion}.
\newblock In \emph{Proceedings of the Twenty-Ninth AAAI Conference on
  Artificial Intelligence}, AAAI'15, pages 2181--2187, Austin, Texas. AAAI
  Press.

\bibitem[{Liu et~al.(2017)Liu, Wu, and Yang}]{analogy}
Hanxiao Liu, Yuexin Wu, and Yiming Yang. 2017.
\newblock Analogical inference for multi-relational embeddings.
\newblock \emph{arXiv preprint arXiv:1705.02426}.

\bibitem[{Schlichtkrull et~al.(2017)Schlichtkrull, Kipf, Bloem, Berg, Titov,
  and Welling}]{rgcn}
Michael Schlichtkrull, Thomas~N Kipf, Peter Bloem, Rianne van~den Berg, Ivan
  Titov, and Max Welling. 2017.
\newblock Modeling relational data with graph convolutional networks.
\newblock \emph{arXiv preprint arXiv:1703.06103}.

\bibitem[{Trouillon et~al.(2016)Trouillon, Welbl, Riedel, Gaussier, and
  Bouchard}]{complex}
Th{\'e}o Trouillon, Johannes Welbl, Sebastian Riedel, \'{E}ric Gaussier, and
  Guillaume Bouchard. 2016.
\newblock \href {http://dl.acm.org/citation.cfm?id=3045390.3045609} {Complex
  embeddings for simple link prediction}.
\newblock In \emph{Proceedings of the 33rd International Conference on
  International Conference on Machine Learning - Volume 48}, ICML'16, pages
  2071--2080. JMLR.org.

\bibitem[{Wang et~al.(2017)Wang, Mao, Wang, and Guo}]{embedding_review}
Quan Wang, Zhendong Mao, Bin Wang, and Li~Guo. 2017.
\newblock Knowledge graph embedding: A survey of approaches and applications.
\newblock \emph{IEEE Trans. Knowl. Data Eng.}, 29(12):2724--2743.

\bibitem[{Wang et~al.(2014)Wang, Zhang, Feng, and Chen}]{transh}
Zhen Wang, Jianwen Zhang, Jianlin Feng, and Zheng Chen. 2014.
\newblock \href {http://dl.acm.org/citation.cfm?id=2893873.2894046} {Knowledge
  graph embedding by translating on hyperplanes}.
\newblock In \emph{Proceedings of the Twenty-Eighth AAAI Conference on
  Artificial Intelligence}, AAAI'14, pages 1112--1119, Qu\&\#233;bec City,
  Qu\&\#233;bec, Canada. AAAI Press.

\bibitem[{Yang et~al.(2014)Yang, Yih, He, Gao, and Deng}]{distmult}
Bishan Yang, Wen-tau Yih, Xiaodong He, Jianfeng Gao, and Li~Deng. 2014.
\newblock Embedding entities and relations for learning and inference in
  knowledge bases.
\newblock \emph{arXiv preprint arXiv:1412.6575}.

\end{thebibliography}
