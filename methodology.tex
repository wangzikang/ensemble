\section{Methodology}

\subsection{Problem Definition}
Denote a knowledge graph as $\mathcal{G} = \{(h,r,t)\} \subseteq \mathcal{E} \times \mathcal{R} \times \mathcal{E}$,
which is a set of triples $(h,r,t)$,
where $h, t \in \mathcal{E}$ and $r \in \mathcal{R}$.
$\mathcal{E}$ denotes the set of entities and $\mathcal{R}$ denotes the set of relations.
Triple $(h,r,t)$ means that entity $h$ and entity $t$ have relation $r$,
with the directed relation $r$ pointing $t$ from $h$.

Link prediction aims to predict the missing entities in a triple $(h, r, t)$.
There are two forms of link prediction task for a triple $(h, r, t)$:
predicting tail entity when given head entity and relation, denoted as $(h,r,?)$,
and predicting head entity when given tail entity and relation, denoted as $(?, r,t)$.

\subsection{Model}

The whole framework is shown in Fig \ref{architecture}.
In the following, we take query $(h, r, ?)$ as an example,
query $(?, r, t)$ is treated in the same way.

%\input{NAACL2019_ensemble/figArchitecture.tex}
\input{figArchitecture}

\subsubsection{Original Ranking}

For a given triple $(h, r, t)$,
existing models first learn embeddings $v_h, v_r, v_t$ for $h,r,t$ respectively.
Then a score $s(h, r, t)$ is learned to evaluate the credibility of it 
based on the learned embedding according to the score function
$f: s(h,r,t) = f(v_{h}, v_{r}, v_{t})$.
A higher score indicates the triple stands with a higher probability.
For each task, take $(h_q,r_q,?)$ as an example,
a score for each triple in set $\mathcal{S}' = \{(h_q,r_q,t_i)| t_i \in
\mathcal{E}\}$ is learned,
where $t_i$ can be every entity in the entity set $\mathcal{E}$.
The known entity and relations $(h_q, r_q)$ is denoted as query $q$.
For each triple, $\left|\mathcal{E} \right|$ scores can be learned,
where $\left|\mathcal{E} \right|$ denotes the size of $\mathcal{E}$.
Then, these scores are ranked in the descending order,
a higher rank indicates a higher probability to be true.

For a given model $M$,
we perform the original procedure it requires.
For triple $(h_q,r_q,?)$, we record the entity ranking list 
$t_q = [t_1, t_2, \dots, t_K], K\in [1,\left|\mathcal{E} \right|]$.
%$t_q$ is a ranked list in descending order,
$t_q$ is an ranked entity list corresponding to scores in descending order,
with $t_1$ denotes the entity with the highest score.
$K$ is a hyperparameter.
We will use these top $K$ entities to improve the link prediction performance.
We also record the embeddings get by model $M$,
which are used in the following ensemble and re-ranker modules. 

\subsubsection{Ensemble} 

The ensemble module consists of two attention layers: a multi-head self-attention layer and an attention layer.

\noindent\textbf{Self-Attention Layer} 
We use a multi-head self-attention layer to capture the overall information in top K entities
and the knowledge between top K entities.
The multi-head mechanism enables the model to learn different types of information.

The self-attention layer updates embeddings of entities by performing a weighted
sum over all top $K$ entities.
Each $v_{t_i}$ is projected to a key $k$, value $v$, and query $q$ with distinct affine transformations following ReLU activation.
Denote $head_{ih}$ as weight for head $h$ of entity $i$,
which is calculated using the scaled dot-product:
\begin{align}
head_{ih} = \sum_{j\in[1,K]} v_{jh}
	\sigma\left(\frac{q_{ih}^Tk_{jh}}{\sqrt{d}}\right),
\end{align}
\noindent where $d$ is the dimension of entities,
$h \in (1, N]$ and $N$ denotes the number of heads.

The attention heads are then concatenated and multiplied with weight parameter $W^{O}$ 
to get the updated entity embedding $v'_{t_i}$, following
\begin{align}
	v'_{t_i} = \text{Concat}(head_{i1};\cdots;head_{iN})W^{O} .
\end{align}

\noindent \textbf{Attention Layer} 
We use an attention layer to aggregate information of the top $K$ entities
with attention assigned according to the query.
The overall aggregated top entity vector is
\begin{align}
    v_t = \sum_{t_i \in t_q}\alpha_{qi} v'_{t_i}  ,
\end{align}
\noindent where $\alpha_{qi}$ is the attention assigned to entity $t_i$,
\begin{align} 
	\alpha_{qi} = \frac{\exp{(g(v_q, v'_{t_i}))}}{\sum_{j=1}^K \exp{(g(v_q,
	v'_{t_j}))}} ,
\end{align}
\noindent where $v_q$ is a query embedding formed by known entity $h_q$ and relation $r_q$, 
in this paper $v_q$ is the concatenation of $v_{h_q}$ and $v_{r_q}$.
$\exp{(g(v_q, v'_{t_i}))}$ is the coefficient get according to a feed-forward neural network $g$.


\subsubsection{Re-ranker}
With query $v_q$ and aggregated entity embedding $v_t$,
we derive a new re-ranking list by feeding $v_q$ and $v_t$ into a re-ranker.
To get a new query with more information,
we concatenate $v_q$ and $v_t$,
and project it into a $d$-dimensional space by linear transformation,
then calculate scores for possible triples to re-rank entities.

To accelerate training and evaluation,
we take one query and score it against all entities simultaneously as in \cite{conve}.
We match new query embedding with the entity embedding matrix $v_{\mathcal{E}}$ via an inner product,
where $v_{\mathcal{E}} \in \mathcal{R}^{\left|\mathcal{E} \right| \times d}$,
and rectified linear units as function $f$ is used.
Then the score vector $\psi(v_{q}, v_{t}) \in \mathbb{R}^{1\times
\left|\mathcal{E} \right|}$ is
\begin{align}
    \psi(v_{q}, v_{t}) = f((v_q \parallel v_t)W)v_{\mathcal{E}}^{T} .
\end{align}
During training,
we apply the logistic sigmoid function $\sigma(\cdot)$ to the score,
set $p = \sigma(\psi_r(v_{q}, v_{t}))$,
and minimize the binary cross-entropy loss:
\begin{align}
	\mathcal{L}(p, t) = &-\frac{1}{N} \sum_{i\in(1,| \mathcal{E}|]} (t_i \cdot \log{(p_i)} + \notag \\
&(1-t_i) \cdot \log{(1-p_i)}) ,
\end{align}
where $t$ is the label vector,
$t_i = 1$ if the corresponding triple is true,
otherwise $t_i=0$.
During the evaluation,
we rank the scores in descending order,
the corresponding entity order list is the re-ranked prediction results.

